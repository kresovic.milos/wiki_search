import 'package:wiki_search/domain/model/data_result.dart';
import 'package:wiki_search/domain/model/search_item.dart';
import 'package:wiki_search/domain/usecases/search_usecase.dart';
import 'package:wiki_search/ui/base/base_viewmodel.dart';

class SearchViewModel extends BaseViewModel {
  final SearchUsecase searchUsecase;

  SearchViewModel(this.searchUsecase);

  List<SearchItem> items = List<SearchItem>();
  Failure<List<SearchItem>> error;
  SearchItem _selectedItem;

  SearchItem get selectedItem => _selectedItem;
  set selectedItem(SearchItem value) {
    _selectedItem = value;
    forceNotify();
  }

  @override
  Future init() async {}

  search(String searchText) async {
    if (searchText.isEmpty) selectedItem = null;
    final results = await load(searchUsecase.execute(searchText));

    if (results?.isFailure() ?? false) {
      error = results;
    } else if (results != null) {
      error = null;
      items = (results as Success<List<SearchItem>>).data;
      selectedItem = items.length > 0 ? items[0] : null;
    }
    forceNotify();
  }
}
