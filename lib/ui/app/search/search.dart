import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wiki_search/domain/model/data_result.dart';
import 'package:wiki_search/domain/usecases/search_usecase.dart';
import 'package:wiki_search/ui/app/search/search_landscape.dart';
import 'package:wiki_search/ui/app/search/search_portrait.dart';
import 'package:wiki_search/ui/app/search/search_textfield/search_textfield.dart';
import 'package:wiki_search/ui/app/search/search_viewmodel.dart';
import 'package:wiki_search/ui/base/base_widget.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Wiki Search'),
        backgroundColor: Colors.grey,
      ),
      body: SafeArea(
        child: BaseWidget<SearchViewModel>(
          viewModel: SearchViewModel(
            Provider.of<SearchUsecase>(context),
          ),
          builder: (ctx, viewModel, child) => Column(
            children: <Widget>[
              SearchTextField(),
              _buildContent(
                viewModel.busy,
                viewModel.error,
                mediaQuery,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildContent(bool busy, Failure error, MediaQueryData mediaQuery) {
    final hasDrawer = mediaQuery.size.width > 600 ||
        mediaQuery.orientation == Orientation.landscape;

    if (busy) {
      return Expanded(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    if (error != null) {
      return Expanded(
        child: Center(
          child: Text(
            error.exception.toString(),
          ),
        ),
      );
    }
    return hasDrawer ? SearchLandscape() : SearchPortrait();
  }
}
