import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:wiki_search/common/constants/resources_keys.dart';
import 'package:wiki_search/domain/model/search_item.dart';
import 'package:wiki_search/localizations/app_localizations.dart';
import 'package:wiki_search/ui/app/search/search_viewmodel.dart';

class SearchPageDetails extends StatefulWidget {
  @override
  _SearchPageDetailsState createState() => _SearchPageDetailsState();
}

class _SearchPageDetailsState extends State<SearchPageDetails> {
  WebViewController _controller;

  @override
  Widget build(BuildContext context) {
    final SearchViewModel viewModel = Provider.of<SearchViewModel>(context);
    final SearchItem selectedItem = viewModel.selectedItem;
    if (selectedItem != null && _controller != null) {
      setState(() {
        _controller.loadUrl(selectedItem.wikiLink);
      });
    }
    if (selectedItem != null) {
      return WebView(
        initialUrl: selectedItem.wikiLink,
        onWebViewCreated: (controller) => _controller = controller,
      );
    } else {
      return Padding(
        padding: const EdgeInsets.only(
          left: 120.0,
          top: 100.0,
        ),
        child: Text(
          AppLocalizations.of(context).translate(
            TextRes.noSelectedItem,
          ),
          style: TextStyle(
            fontSize: 20,
          ),
        ),
      );
    }
  }
}
