import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wiki_search/ui/app/search/search_result/search_result_item/search_result_item.dart';
import 'package:wiki_search/ui/app/search/search_viewmodel.dart';

class SearchPageMaster extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final SearchViewModel viewModel = Provider.of<SearchViewModel>(context);

    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: viewModel.items.length,
      itemBuilder: (BuildContext context, int index) =>
          SearchResultItem(result: viewModel.items[index]),
    );
  }
}
