import 'package:flutter/material.dart';
import 'package:wiki_search/domain/model/search_item.dart';
import 'package:wiki_search/ui/app/search/search_result/search_result_item/search_result_item_landscape.dart';
import 'package:wiki_search/ui/app/search/search_result/search_result_item/search_result_item_portrait.dart';

class SearchResultItem extends StatefulWidget {
  final SearchItem result;
  SearchResultItem({Key key, this.result}) : super(key: key);

  @override
  _SearchResultItemState createState() => _SearchResultItemState();
}

class _SearchResultItemState extends State<SearchResultItem> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final hasDrawer = mediaQuery.size.width > 600 ||
        mediaQuery.orientation == Orientation.landscape;
    return hasDrawer
        ? SearchResultItemLandscape(
            item: widget.result,
          )
        : SearchResultItemPortrait(
            item: widget.result,
          );
  }
}
