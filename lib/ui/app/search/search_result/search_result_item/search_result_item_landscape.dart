import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wiki_search/domain/model/search_item.dart';
import 'package:wiki_search/ui/app/search/search_viewmodel.dart';

class SearchResultItemLandscape extends StatelessWidget {
  final SearchItem item;

  const SearchResultItemLandscape({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final SearchViewModel viewModel = Provider.of<SearchViewModel>(context);
    return ListTile(
      selected: viewModel.selectedItem != null &&
          item.title == viewModel.selectedItem.title,
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            item != null ? '${item.title}' : 'no data',
          ),
        ],
      ),
      onTap: () {
        final SearchViewModel viewModel = Provider.of<SearchViewModel>(context);
        viewModel.selectedItem = item;
      },
    );
  }
}
