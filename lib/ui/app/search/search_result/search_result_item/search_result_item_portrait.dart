import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wiki_search/domain/model/search_item.dart';

class SearchResultItemPortrait extends StatelessWidget {
  final SearchItem item;

  const SearchResultItemPortrait({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            '${item.title} (${item.images.length} picture${item.images.length > 1 ? 's' : ''})',
          ),
          Text(
            item.wikiLink,
            style: TextStyle(fontSize: 12),
          ),
        ],
      ),
      subtitle: SizedBox(
        height: 100.0,
        child: Scrollbar(
          child: ListView.builder(
            physics: ClampingScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: item.images.length,
            itemBuilder: (BuildContext context, int index) => Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CachedNetworkImage(
                  imageUrl: item.images[index],
                  placeholder: (
                    context,
                    url,
                  ) =>
                      Icon(Icons.image),
                  errorWidget: (
                    context,
                    url,
                    error,
                  ) =>
                      Icon(Icons.error),
                ),
              ),
            ),
          ),
        ),
      ),
      onTap: () => launch(item.wikiLink),
    );
  }
}
