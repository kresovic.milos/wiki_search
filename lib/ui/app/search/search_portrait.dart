import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wiki_search/ui/app/search/search_result/search_result_master.dart';

class SearchPortrait extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SearchPageMaster(),
    );
  }
}
