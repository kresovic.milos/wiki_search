import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wiki_search/common/constants/resources_keys.dart';
import 'package:wiki_search/localizations/app_localizations.dart';
import 'package:wiki_search/ui/app/search/search_viewmodel.dart';

class SearchTextField extends StatefulWidget {
  SearchTextField({Key key}) : super(key: key);

  @override
  _SearchTextFieldState createState() => _SearchTextFieldState();
}

class _SearchTextFieldState extends State<SearchTextField> {
  final TextEditingController searchController = TextEditingController();
  Timer _debounce;

  _onSearchChanged() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      Provider.of<SearchViewModel>(context).search(
        searchController.text.trim().toLowerCase(),
      );
    });
  }

  @override
  void initState() {
    super.initState();
    searchController.addListener(_onSearchChanged);
  }

  @override
  void dispose() {
    searchController.removeListener(_onSearchChanged);
    searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: searchController,
      // focusNode: searchContactsFocus,
      decoration: InputDecoration(
        fillColor: Colors.white,
        filled: true,
        prefixIcon: Icon(
          Icons.search,
          color: Colors.grey,
        ),
        suffixIcon: searchController.text.length == 0
            ? SizedBox(
                height: 0,
              )
            : GestureDetector(
                onTap: () {
                  WidgetsBinding.instance.addPostFrameCallback((_) => {
                        FocusScope.of(context).unfocus(),
                        searchController.text = ''
                      });
                },
                child: Icon(Icons.cancel, color: Colors.grey)),
        hintText: AppLocalizations.of(context).translate(TextRes.searchWiki),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12.0)),
      ),
    );
  }
}
