import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wiki_search/ui/app/search/search_result/search_result_details.dart';
import 'package:wiki_search/ui/app/search/search_result/search_result_master.dart';

class SearchLandscape extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Flexible(
            flex: 1,
            fit: FlexFit.loose,
            child: SearchPageMaster(),
          ),
          Expanded(
            flex: 3,
            child: SearchPageDetails(),
          ),
        ],
      ),
    );
  }
}
