class SearchItem {
  final String title;
  final String wikiLink;
  final List<String> images;

  SearchItem({this.title, this.wikiLink, this.images});
}
