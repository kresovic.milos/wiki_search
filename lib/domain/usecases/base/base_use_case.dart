import 'package:wiki_search/domain/model/data_result.dart';

abstract class BaseUseCase<P, R> {
  Future<DataResult<R>> execute(P params);
}
