import 'package:wiki_search/data/repositories/search/search_repo.dart';
import 'package:wiki_search/domain/model/search_item.dart';
import 'package:wiki_search/domain/usecases/base/base_use_case.dart';

class SearchUsecase extends BaseUseCase<String, List<SearchItem>> {
  final SearchRepo _searchRepo;

  SearchUsecase(this._searchRepo);

  @override
  execute(params) async => _searchRepo.search(params);
}
