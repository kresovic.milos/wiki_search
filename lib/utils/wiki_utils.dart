import 'package:wiki_search/common/constants/api.dart';

prepareGetImagesParams(String query, String title) => {
      ReqParams.action: 'query',
      ReqParams.search: query,
      ReqParams.format: 'json',
      ReqParams.prop: 'pageimages',
      ReqParams.titles: title,
      ReqParams.generator: 'images',
      ReqParams.piprop: 'thumbnail',
      ReqParams.gimlimit: 30,
    };
