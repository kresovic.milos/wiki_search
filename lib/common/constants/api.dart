abstract class HttpConfig {
  static const String baseApiUrl = 'https://en.wikipedia.org/w/api.php';
  static const int connectTimeoutInMs = 5000;
  static const int receiveTimeoutInMs = 3000;
}

abstract class Endpoints {
  static const String root = '/';
}

abstract class ReqParams {
  static const String action = 'action';
  static const String search = 'search';
  static const String format = 'format';
  static const String prop = 'prop';
  static const String generator = 'generator';
  static const String piprop = 'piprop';
  static const String gimlimit = 'gimlimit';
  static const String titles = 'titles';
}
