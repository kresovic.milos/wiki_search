import 'package:wiki_search/data/repositories/base/base_repo.dart';
import 'package:wiki_search/domain/model/data_result.dart';
import 'package:wiki_search/domain/model/search_item.dart';

abstract class SearchRepo implements BaseRepo {
  Future<DataResult<List<SearchItem>>> search(String query);
}
