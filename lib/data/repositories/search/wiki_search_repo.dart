import 'package:dio/dio.dart';
import 'package:wiki_search/common/constants/api.dart';
import 'package:wiki_search/data/repositories/search/search_repo.dart';
import 'package:wiki_search/domain/model/data_result.dart';
import 'package:wiki_search/domain/model/search_item.dart';
import 'package:wiki_search/utils/wiki_utils.dart';

class WikiSearchRepo implements SearchRepo {
  final Dio _http;

  WikiSearchRepo(this._http);

  @override
  void dispose() {
    _http.close();
  }

  Map<String, String> imageUrlMap = Map<String, String>();

  @override
  Future<DataResult<List<SearchItem>>> search(String query) async {
    List<SearchItem> data = List<SearchItem>();

    if (query == '') {
      return Future.value(DataResult.success(data));
    }

    final response = await _http.get(Endpoints.root, queryParameters: {
      ReqParams.action: 'opensearch',
      ReqParams.search: query,
    });

    List<dynamic> titlesList = response.data[1];
    if (titlesList.length == 0) {
      return Future.value(DataResult.success(data));
    }
    List<dynamic> pageLinksList = response.data[3];

    final imagesResponses = await Future.wait(
      titlesList.map(
        (title) => _http.get(
          Endpoints.root,
          queryParameters: prepareGetImagesParams(query, title),
        ),
      ),
    );

    for (int i = 0; i < titlesList.length; i++) {
      final imagesResponse = imagesResponses[i].data;
      final query = imagesResponse['query'];
      if (query == null) {
        return Future.value(DataResult.failure(Exception('No data')));
      }
      final Map<String, dynamic> pages = query['pages'];
      final List<String> imagesUrls = pages.values.map((img) {
        final original = img['thumbnail'];
        final String source = original['source'];
        return source;
      }).toList();

      data.add(
        SearchItem(
          title: titlesList[i].toString(),
          wikiLink: pageLinksList[i].toString(),
          images: imagesUrls.where((image) => !image.contains('.svg')).toList(),
        ),
      );
    }
    return DataResult.success(data);
  }
}
