import 'package:dio/dio.dart';
import 'package:wiki_search/data/repositories/base/base_repo.dart';
import 'package:wiki_search/data/repositories/search/search_repo.dart';
import 'package:wiki_search/data/repositories/search/wiki_search_repo.dart';

abstract class RepoFactory {
  static final _factories = <Type, Function>{
    SearchRepo: (Dio httpClient) => WikiSearchRepo(httpClient),
  };

  static T createRepo<T extends BaseRepo, P>(P param) {
    return _factories[T](param);
  }
}
