abstract class BaseConverter<DO, DA> {
  DA toDataModel(DO domainModel);
  DO toDomainModel(DA dataModel);
}
