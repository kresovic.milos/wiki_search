import 'package:dio/dio.dart';
import 'package:provider/provider.dart';
import 'package:wiki_search/common/constants/api.dart';
import 'package:wiki_search/data/repositories/base/repo_factory.dart';
import 'package:wiki_search/data/repositories/search/search_repo.dart';
import 'package:wiki_search/domain/usecases/search_usecase.dart';

List<SingleChildCloneableWidget> independentProviders = [
  Provider<Dio>.value(
    value: Dio(
      BaseOptions(
        baseUrl: HttpConfig.baseApiUrl,
        connectTimeout: HttpConfig.connectTimeoutInMs,
        receiveTimeout: HttpConfig.receiveTimeoutInMs,
      ),
    ),
  ),
];

final List<SingleChildCloneableWidget> dependentProviders = [
  ProxyProvider<Dio, SearchRepo>(
      builder: (
    context,
    httpClient,
    instance,
  ) =>
          instance ?? RepoFactory.createRepo<SearchRepo, Dio>(httpClient)),
  ProxyProvider<SearchRepo, SearchUsecase>(
    builder: (
      context,
      repo,
      instance,
    ) =>
        instance ?? SearchUsecase(repo),
  ),
];
